#!/python3
"""
A demo of reading, parsing, and acting on a stream using Python 'threads'.
The goal is to be able to read from the stream when new data is available,
and share that data with the 'actor' so that it can use the most current 
information.
Here the main issue is being able to hand off control to the reader at 
appropriate intervals so that it can check whether the stream has new data.
Parsing the data is assumed to be quick, and the actor is assumed to be able
to make decisions within a relatively short timeframe, so both threads spend
most of their time idle.
If I/O is not the bottleneck (data acquisition or processing takes too long)
then an alternative method will likely be necessary.

For compactness, it also implements a stream writer using a pseudo tty.
"""
import io
import logging
import os
import pty
import select
import threading
import time
import serial

from collections import deque
from itertools import cycle
from threading import Thread


# Simple logging
log = logging
log.basicConfig(level=logging.DEBUG)



class SimpleStream:
    """Class for opening a pseudo tty and writing repeating data to it for use
    as a proof-of-concept when real streams are unavailable/inconvenient.
    """
    def __init__(self, interval=1):
        self.interval = interval

        # Create a pseudo terminal
        self.master, self.slave = pty.openpty()
        self.slave_name = os.ttyname(self.slave)

        log.info("Created pty: %s"%self.slave_name)

        # The generator for creating data
        self.gen = cycle((i.to_bytes(1, 'big') for i in range(256)))

    def run(self, quit_event):
        try:
            log.info("SimpleStream has started.")
            log.info("Started streaming to: %s"%self.slave_name)
            while not quit_event.is_set():
                data = next(self.gen)
                log.info("writing: %s"%data)
                os.write(self.master, data)
                time.sleep(self.interval)
        finally:
            # Perform cleanup
            log.debug("SimpleStream has stopped.")


class DequeParser:
    """Stream parser with a circular buffer implemented with a deque."""
    def __init__(self, stream, bufsize=1024):
        log.info("reading from stream: %s"%stream)
        self.stream = stream
        self.buffer = deque([-1], maxlen=bufsize)
        self.lock   = threading.Lock() # maybe not needed if deque is thread-safe?

        log.debug("parser buffer id: %s"%id(self.buffer))

    def run(self, quit_event):
        try:
            log.info("DequeParser has started.")
            while not quit_event.is_set():
                rlst, wlst, elst = select.select([self.stream], [], [], 0)

                if rlst:
                    data = self.stream.read()
                    log.info("read: %s"%data)
                    val = int.from_bytes(data, 'big')
                    log.info("appending: %s"%val)
                    self.buffer.append(val)
                else:
                    time.sleep(0.001)
        finally:
            # Perform cleanup
            log.debug("DequeParser has stopped.")


class Actor:
    """Actor which performs tasks using information from a buffer."""
    def __init__(self, buffer, interval=1):
        """
        Initialize the actor with a `buffer` to read from and an `interval` to
        wait between acting.
        """
        self.buffer = buffer
        self.interval = interval

        log.debug("actor buffer id: %s"%id(self.buffer))

    def run(self, quit_event):
        """Run the actor until `quit_event` (a `threading.Event` object) is set,
        acting on the most recent data from the actor's `buffer` attribute.
        """
        try:
            log.info("Actor has started.")
            while not quit_event.is_set():
                current = self.buffer[-1]
                log.info("acting based on value: %s"%current)
                time.sleep(self.interval)
        finally:
            # Perform cleanup
            log.debug("Actor has stopped.")

if __name__ == "__main__":
    # Termination event
    quit_flag = threading.Event()
    try:
        # Create & run a dummy stream writer
        writer = SimpleStream(interval=1)

        # Create the stream parser
        ser = serial.Serial(writer.slave_name, timeout=0)
        ser.nonblocking()
        reader = DequeParser(ser, bufsize=1024)
        
        # Create the actor
        actor = Actor(reader.buffer, interval=1)
        
        # Create the threads for each object
        t_writer = Thread(target=writer.run, name="writer", args=(quit_flag,))
        t_reader = Thread(target=reader.run, name="reader", args=(quit_flag,))
        t_actor = Thread(target=actor.run, name="actor", args=(quit_flag,))
        
        # Start the threads
        t_writer.start()
        t_reader.start()
        t_actor.start()
        
        # Do nothing in the main thread
        while True:
            time.sleep(1)
    except KeyboardInterrupt as e:
        # Set the quit flag
        log.debug("Setting quit flag.")
        quit_flag.set()
    finally:
        log.info("Cleaning up...")

        # Join the threads
        tlst = list(threading.enumerate())
        log.debug("Threads: %s"%tlst)
        tlst.remove(threading.main_thread())

        while tlst:
            for t in tlst:
                t.join(0.01)
                if not t.is_alive():
                    tlst.remove(t)
        log.info("Done.")
