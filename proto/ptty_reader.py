"""
Demonstrating reading from a pseudo terminal
"""
import argparse
import logging
import os
import select
import serial
import sys
import time

# Logging, why not
log = logging.getLogger(__name__)
if not log.hasHandlers():
    _handler = logging.StreamHandler()
    _formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-4s %(message)s')
    _handler.setFormatter(_formatter)
    log.addHandler(_handler)
    log.setLevel(logging.DEBUG)


def serial_select(port):
    time.sleep(1)

    ser = serial.Serial(port, timeout=0)
    ser.nonblocking()

    while True:
        rlst, wlst, elst = select.select([ser], [], [], 0)

        if rlst:
            data = ser.read()
            log.info("read: %s" %data)
        else:
            time.sleep(0.001)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Specify a device to read...")
    else:
        port = sys.argv[1]
        log.info("Reading from:", port)
        serial_select(port)