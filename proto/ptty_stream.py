"""
Demonstrating how to create a pseudo terminal and stream data to it.
"""
import argparse
import logging
import os
import pty
import serial
import sys
import time

from packet_generator import PacketStream

# Logging, why not
log = logging.getLogger(__name__)
if not log.hasHandlers():
    _handler = logging.StreamHandler()
    _formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-4s %(message)s')
    _handler.setFormatter(_formatter)
    log.addHandler(_handler)
    log.setLevel(logging.DEBUG)



# Create the pseudo terminal
master, slave = pty.openpty()

# Check that PTTY opened correctly
assert(os.isatty(master) and os.isatty(slave))

# Names of master/slave -- should be /dev/ptmx and /dev/ptyXX
# master_name = os.ttyname(master) # not available on OS X
slave_name = os.ttyname(slave)

# log.debug("master name: %s \t fd:%d"%(master_name, master))
log.debug("slave name: %s \t fd:%d"%(slave_name, slave))

# TODO: Remove the hard-coding, use argparse instead
pkt_ids = [21, 22, 23, 24]
interval = 1.0

# Create the stream
stream = PacketStream(*pkt_ids, interval=interval)

# Indicate the stream's target
print("Streaming to:", slave_name)

# Beging streaming
try:
    while True:
        data = stream.read()
        if data:
            log.debug('writing: %s'%data)
            os.write(master, data)
        else:
            time.sleep(0.05) # REMOVE THIS
except KeyboardInterrupt as e:
    log.debug('exiting due to keyboard interrupt')
finally:
    log.debug('done.')
