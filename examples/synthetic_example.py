"""
A self-contained example to illustrate how the rest of the code works.
It is somewhat long because it implements a packet generator in addition to the
stuff that would actually be relevant for working with robots.

The packet generator is used by the `FakeStream` class to make sample data
for `StreamReader` to read.
Bytes read from the stream are passed to the `Parser` object, which forms them
into packets that get stored in `deque`.
The actor reacts to data from this buffer, and were this demo meant to be run
on a robot, would probably issue commands to the robot according to the values
it is receiving.
"""
# Simple logging
import logging, sys
log = logging.getLogger(__name__)
_log_format = '%(asctime)s %(name)-12s %(levelname)-4s %(message)s'
logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=_log_format)

import argparse
import os
import pty
import random
import select
import struct
import threading
import time
import serial

from collections import deque
from itertools import cycle
from threading import Thread

from cspy3 import Controller, Parser
from cspy3 import v1


class Flag(threading.Event):
    """A wrapper for the typical event class to allow for overriding the
    `__bool__` and `__call__` magic methods, since it looks nicer.
    """
    def __bool__(self):
        return self.is_set()

    def __call__(self):
        return self.is_set()


class PacketGenerator:
    """A generator of mock packets for debugging."""
    FIRST_BYTE = 19
    def __init__(self, *sensor_ids):
        self.sensor_ids = sensor_ids
        self.setup()

    def setup(self):
        """Compute and set information for generating packets."""
        sensor_ids = self.sensor_ids
        # Get information necessary to build packets
        self.info   = [v1.PACKETS[x] for x in sensor_ids]
        self.sizes  = [x['size'] for x in self.info]
        self.names  = [x['name'] for x in self.info]
        self.dtypes = [x['dtype'] for x in self.info]
        self.ranges = [x['ValueRange'] for x in self.info]
        self.nbytes  = sum(self.sizes) + len(self.sensor_ids)
        self.format = ">BBB" + "B".join(self.dtypes) + "B"
        self.length = self.nbytes + 3

        log.debug('names: %s'%self.names)
        log.debug('sizes: %s'%self.sizes)
        log.debug('dtypes: %s'%self.dtypes)
        log.debug('format: %s'%self.format)
        log.debug('number of sensors: %d'%len(self.sensor_ids))
        log.debug('number of sensor bytes: %d'%self.nbytes)
        log.debug('packet length: %d'%self.length)

    def make_packet(self):
        """Create a packet, generating the data and converting it to bytes"""
        data = [self.FIRST_BYTE, self.nbytes]
        for sid, rng in zip(self.sensor_ids, self.ranges):
            val = random.randint(*rng)
            data.extend([sid, val])
        ret = bytearray(struct.pack(self.format[:-1], *data))
        ret.append(-sum(ret)%256)
        log.debug("Making packet from: %s"%data)
        return ret

    def __iter__(self):
        return self

    def __next__(self):
        return self.make_packet()


class FakeStream:
    """Create a fake stream."""
    def __init__(self, generator, interval=1):
        log.debug("Creating FakeStream object")
        self.generator = generator
        self.interval = interval

        log.debug("Opening a fake stream...")
        self.master, self.slave = pty.openpty()
        self.slave_name = os.ttyname(self.slave)
        log.debug("Streaming to: %s"%self.slave_name)

    def run(self, halt_condition=None):
        if halt_condition is None:
            halt_condition = False

        try:
            self.tic = time.time()
            while not halt_condition:
                while time.time() < self.tic:
                    time.sleep(0.0001)
                self.tic += self.interval

                msg = next(self.generator)
                log.debug("writing: %s"%msg)
                os.write(self.master, msg)

        finally:
            log.debug("Cleaning up FakeStream...")
            os.close(self.slave)
            os.close(self.master)
            log.debug("FakeStream stopped.")


class StreamReader:
    """Reads streaming data using serial, passes it to a handler."""
    def __init__(self, stream, handler):
        log.debug("Creating StreamReader object for stream: %s"%stream)
        self.stream = stream
        self.handler = handler

    def run(self, halt_condition=None):
        """Begin the stream input processing loop."""
        if halt_condition is None:
            halt_condition = False

        try:
            log.debug("StreamReader started.")
            while not halt_condition:
                rlst, wlst, elst = select.select([self.stream], [], [], 0)

                if rlst:
                    data = self.stream.read()
                    log.debug("read: %s"%data)
                    self.handler(*data)
                else:
                    time.sleep(0.001)
        finally:
            # Perform cleanup
            log.debug("StreamReader stopped.")


class Actor:
    """Acts on data that has been processed.

    It has a feed (e.g., some sort of thread-safe data structure) that supplies
    it with observations, which it then acts on.
    Were it controlling a real robot, these actions might consist of sending
    messages back to the robot, telling it to do things.
    """
    def __init__(self, feed, interval=1):
        log.debug("Creating Actor object")
        self.feed = feed
        self.interval = interval

    def run(self, halt_condition=None):
        """Begin the action loop."""
        if halt_condition is None:
            halt_condition = False

        # wait for data to become available
        while not self.feed and not halt_condition:
            log.debug("Actor waiting for feed to become available")
            log.debug(self.feed)
            time.sleep(self.interval)
        else:
            if self.feed:
                obs = self.feed.pop()

        # begin reading and reacting to data
        tic = time.time()
        while not halt_condition:
            while time.time() < tic:
                time.sleep(0.0001)
            tic += self.interval

            # Get the most recent data from the feed, discarding old ones
            while self.feed:
                obs = self.feed.pop()

            # Do something with the information
            log.debug("Recent observation: %s"%obs)


if __name__ == "__main__":
    flag = Flag()
    try:
        sensor_ids = [20, 21, 22, 23, 24]
        sensor_ids = [29, 13]

        # stream writer
        pktgen = PacketGenerator(*sensor_ids)
        writer = FakeStream(pktgen, interval=0.5)

        # the fake stream
        ser = serial.Serial(writer.slave_name, timeout=0)
        ser.nonblocking()

        # the stream parser and output data store
        parser = Parser(*sensor_ids)
        output = parser.output

        # stream reader
        reader = StreamReader(ser, parser)

        # actor/controller
        actor = Actor(output, interval=1)

        # Set up threads
        t_writer = Thread(target=writer.run, name="writer", args=(flag,))
        t_writer.start()

        t_reader = Thread(target=reader.run, name="reader", args=(flag,))
        t_reader.start()

        t_actor = Thread(target=actor.run, name="actor", args=(flag,))
        t_actor.start()

        # List of all child threads
        threads = list(threading.enumerate()).remove(threading.main_thread())

        # Main thread just sleeps
        while True:
            time.sleep(0.1)


    except KeyboardInterrupt:
        log.debug("Interrupt received, setting quit flag.")
        flag.set()
    finally:
        # Ensure that the flag is set regardless since program is terminating
        log.debug("Starting termination, setting quit flag.")
        flag.set()

        # Join the threads
        log.debug("Attempting to join threads...")
        while threads:
            for t in threads:
                t.join(0.1)
                if t.is_alive():
                    log.debug("Thread %s not ready to join"%t.name)
                else:
                    log.debug("Thread %s successfully joined"%t.name)
                    threads.remove(t)
        log.debug("Program terminated.")